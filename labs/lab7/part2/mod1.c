/* **************** LFD331:3.13 s_05/sample_driver.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2014
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/cdev.h>         /* cdev utilities */
#include <linux/miscdevice.h>   /* misc driver utils */

#include <linux/interrupt.h>

#define MYDEV_NAME "mycdrv"
#define INTNUM 19

int intcount = 0;
int dev_id;
const char *intname = "mod1_interrupts";

irqreturn_t handler(int irq, void *dev_id)
{
    intcount++;
    return(IRQ_NONE);
}


static ssize_t
mycdrv_read(struct file *file, char __user * buf, size_t lbuf, loff_t * ppos)
{
#define MYBUFSIZE 20
        int nbytes;
        char mybuf[MYBUFSIZE];
        int copylen;
        nbytes = snprintf(mybuf, MYBUFSIZE-1, "%d interrupts\n",intcount);
        pr_info("read:  *ppos = %d\n",(int)*ppos);
        if(*ppos > nbytes){
        	return 0;
        }
        copylen = MYBUFSIZE-*ppos;
       	if (copylen > lbuf)
       	{
       		copylen = lbuf;
       	}
        pr_info("returning %d bytes\n", (int)lbuf);
        nbytes = lbuf - copy_to_user(buf, mybuf + *ppos, copylen);
        *ppos += nbytes;
        pr_info("\n READING function, nbytes=%d, pos=%d\n", nbytes, (int)*ppos);
        return nbytes;
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .read = mycdrv_read,
};

static struct miscdevice mdevice = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = "intmonitor2",
    .fops = &mycdrv_fops,
    .nodename = "intmonitornode",
    .mode = 0666
};


static int __init my_init(void)
{

        pr_info("my_init\n");
        misc_register(&mdevice);
        request_irq(INTNUM, handler, IRQF_SHARED, intname, &dev_id);

        return 0;
}

static void __exit my_exit(void)
{

		synchronize_irq(INTNUM);
        free_irq(INTNUM, &dev_id);
        misc_deregister(&mdevice);
        pr_info("mod1 interrupts counted:  %d\n", intcount);
        pr_info("\ndevice unregistered\n");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Jerry Cooperstein");
MODULE_LICENSE("GPL v2");
