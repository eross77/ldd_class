/* **************** LFD331:3.13 s_05/sample_driver.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2014
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/cdev.h>         /* cdev utilities */
#include <linux/miscdevice.h>   /* misc driver utils */
#include <linux/mutex.h>

#define MYDEV_NAME "mycdrv"
#define INTNUM 19

DEFINE_MUTEX(mymutex);

EXPORT_SYMBOL(mymutex);

static int __init my_init(void)
{
       /*  misc_register(&mdevice); */
        pr_info("my_init\n");

        return 0;
}

static void __exit my_exit(void)
{
        /* misc_deregister(&mdevice); */
        pr_info("device unregistered\n");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Eric Ross");
MODULE_LICENSE("GPL v2");
