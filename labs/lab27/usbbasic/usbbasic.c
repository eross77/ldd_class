/* **************** LFD331:3.13 s_05/sample_driver.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2014
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/cdev.h>         /* cdev utilities */
#include <linux/miscdevice.h>   /* misc driver utils */
#include <linux/usb.h>

#define MYDEV_NAME "mycdrv"
#define INTNUM 19

int intcount = 0;

static const struct file_operations usb_my_fops = {
    .owner = THIS_MODULE
};

static struct usb_class_driver usb_my_class = {
    .name = "myusb%d",
    .fops = &usb_my_fops,
    .minor_base = 0
};

static struct usb_device_id my_usb_id_table[] = {
    {USB_DEVICE(0x18d1, 0x4ee1)},
    {}
};

MODULE_DEVICE_TABLE(usb, my_usb_id_table);

static int
my_usbprobe(struct usb_interface *intf,
            const struct usb_device_id *id)
{
    int retval;
    struct usb_device *dev = interface_to_usbdev(intf);
    dev_info(&intf->dev, "MY USB found at address %d\n", dev->devnum);
    retval = usb_register_dev(intf, &usb_my_class);
    return 0;
}

static void
my_usbdisconnect(struct usb_interface *intf)
{
    struct usb_device *dev = interface_to_usbdev(intf);
    dev_info(&intf->dev, "Disconnecting MY USB at address %d\n", dev->devnum);
    usb_deregister_dev(intf, &usb_my_class);
    return;
}

static struct usb_driver my_usb_driver = {
    .name = "my_usb_device",
    .id_table = my_usb_id_table,
    .probe = my_usbprobe,
    .disconnect = my_usbdisconnect
};


module_usb_driver(my_usb_driver);

#if 0
static int __init my_init(void)
{
 
        pr_info("my_init\n");

        return 0;
}

static void __exit my_exit(void)
{
        misc_deregister(&mdevice);
        pr_info("device unregistered\n");
}
#endif
//module_init(my_init);
//module_exit(my_exit);

MODULE_AUTHOR("Eric Ross");
MODULE_LICENSE("GPL v2");
