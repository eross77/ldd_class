/* **************** LFD331:3.13 s_05/sample_driver.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2014
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/cdev.h>         /* cdev utilities */
#include "mod2.h"

#define MYDEV_NAME "mycdrv"

static char *ramdisk;
#define ramdisk_size (size_t) (16*PAGE_SIZE)
#define ANSWERSIZE 20

static dev_t first;
static unsigned int count = 1;
static struct cdev *my_cdev;
static int open_count = 0;
static char *mydata = NULL;
static dev_t first;



static int mycdrv_open(struct inode *inode, struct file *file)
{
        pr_info(" OPENING device: %s:%d\n\n", MYDEV_NAME,++open_count);
        mydata = (char *)kmalloc(10, __GFP_WAIT);
        return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
        pr_info(" CLOSING device: %s:\n\n", MYDEV_NAME);
        if(mydata != NULL)
        {
            kfree(mydata);
        }
        return 0;
}

static ssize_t
mycdrv_read(struct file *file, char __user * buf, size_t lbuf, loff_t * ppos)
{
        char mybuf[ANSWERSIZE];
        int nbytes;
        int strsize = 0;
        pr_info("\n READ FCN CALLED, lbuf = %d, *ppos = %d\n",(int)lbuf, (int)*ppos);
        strsize = snprintf(mybuf, ANSWERSIZE-1, "%d",mod2_answer());
        if ((lbuf + *ppos) > strsize) {
                pr_info("trying to read past end of device,"
                        "aborting because this is just a stub!\n");
                return 0;
        }
        if (lbuf > strsize)
        {
            lbuf = strsize;
        }
        nbytes = lbuf - copy_to_user(buf, mybuf + *ppos, lbuf);
        *ppos += nbytes;
        pr_info("\n READING function, nbytes=%d, pos=%d\n", nbytes, (int)*ppos);
        return nbytes;
}

static ssize_t
mycdrv_write(struct file *file, const char __user * buf, size_t lbuf,
             loff_t * ppos)
{
        int nbytes;
        if ((lbuf + *ppos) > ramdisk_size) {
                pr_info("trying to read past end of device,"
                        "aborting because this is just a stub!\n");
                return 0;
        }
        nbytes = lbuf - copy_from_user(ramdisk + *ppos, buf, lbuf);
        *ppos += nbytes;
        pr_info("\n WRITING function, nbytes=%d, pos=%d\n", nbytes, (int)*ppos);
        return nbytes;
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .read = mycdrv_read,
        .write = mycdrv_write,
        .open = mycdrv_open,
        .release = mycdrv_release,
};

static int __init my_init(void)
{
        ramdisk = kmalloc(ramdisk_size, GFP_KERNEL);
        alloc_chrdev_region(&first, 0, 1, "samplepart2");
        my_cdev = cdev_alloc();
        cdev_init(my_cdev, &mycdrv_fops);
        cdev_add(my_cdev, first, count);
        pr_info("\nSucceeded in registering character device %s\n", MYDEV_NAME);
        pr_info("Major: %d, Minor: %d\n",MAJOR(first), MINOR(first));
        return 0;
}

static void __exit my_exit(void)
{
        cdev_del(my_cdev);
        unregister_chrdev_region(first, count);
        pr_info("\ndevice unregistered\n");
        kfree(ramdisk);
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Jerry Cooperstein");
MODULE_LICENSE("GPL v2");
