/* **************** LFD331:3.13 s_05/sample_driver.c **************** */
/*
 * The code herein is: Copyright the Linux Foundation, 2014
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/* 
Sample Character Driver 
@*/

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/cdev.h>         /* cdev utilities */
#include <linux/miscdevice.h>   /* misc driver utils */
#include <linux/kprobes.h>
#include <linux/interrupt.h>
#include <linux/netdevice.h>

#define MYDEV_NAME "mycdrv"
#define INTNUM 19

int intcount = 0;

static int 
my_net_open(struct net_device *dev)
{
    return 0;
}

static int
my_net_stop(struct net_device *dev)
{
    return 0;
}

static int
my_net_xmit(struct sk_buff *skb, struct net_device *dev)
{
    dev_kfree_skb(skb);
    return 0;
}

static struct net_device_ops ndo = {
    .ndo_open = my_net_open,
    .ndo_stop = my_net_stop,
    .ndo_start_xmit = my_net_xmit
};

static struct net_device *dev = NULL;

static int __init my_init(void)
{       
        dev = alloc_netdev(0, "ericnet", ether_setup);
        dev->netdev_ops = &ndo;
        register_netdev(dev);
        pr_info("my_init: dev = %lx\n", (long)dev);

        return 0;
}

static void __exit my_exit(void)
{
        unregister_netdev(dev);
        free_netdev(dev);
        pr_info("device unregistered\n");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Eric Ross");
MODULE_LICENSE("GPL v2");
