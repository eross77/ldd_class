#include <sys/inotify.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

int main(int argc, char **argv)
{
	char buf[EVENT_BUF_LEN];
	int ifd;
	int wfd;
	int ret;
	ifd = inotify_init();
	if(ifd < 0)
	{
		perror("inotify_init");
		exit(-1);
	}
	//wfd = inotify_add_watch(ifd, "/tmp", IN_CREATE | IN_MODIFY);
	wfd = inotify_add_watch(ifd, "/tmp", IN_ALL_EVENTS);

	while((ret = read(ifd,buf, EVENT_BUF_LEN)) != 0)
	{
		char *pt;
		fprintf(stdout, "size: %d\n", sizeof(struct inotify_event));
		for(pt=buf;pt < (buf+ret);)
		{
			struct inotify_event *evt = (struct inotify_event *)pt;
			pt += sizeof(struct inotify_event) + evt->len;
			fprintf(stdout, "ret: %d\n", ret);
			fprintf(stdout, "Mask: %x\n",evt->mask);
			fflush(stdout);
			fprintf(stdout, "%s\n", evt->name);
		}

	}
	return 0;
}