#!/bin/bash
# 
# Copyright (c) 2013 Chris Simmonds
#               2013-2014 Behan Webster
#               2014 Jan-Simon Möller
#
# Originally written by Chris Simmonds for LF315
# Version 1.0: 18th January 2013
# Massive updates (bordering on a rewrite) by Behan Webster
# Version 1.5: 11th December 2013
#     - Fixed support for Ubuntu-12.04 for LF315
# Version 1.6: 15th December 2013
#     - Parameterized code to make it easier to update
# Version 1.7: 16th December 2013
#     - Added support for other classes
# Version 2.0: 19th December 2013
#     - Added support for multiple distros
#     - Renumbered courses
# Version 2.1: 20th December 2013
#     - Bug fixes
#     - Adjust better defaults for courses
#     - make libGL.so link optional
#     - Add --simulate-failure option for debugging
# Version 2.2: 2nd January 2014
#     - Added JSON support
#     - Add better support for Arch
#     - Add better support for Centos
#     - Add better support for Fedora
#     - Add better support for Gentoo
#     - Add better support for openSUSE
# Version 2.2: 9th January 2014
#     - Changed minimum openSUSE to 12.2
#     - Added PACKAGE macro support (e.g. @embedded, @kernel)
#     - Added package list to JSON output
#     - Added course requirements/packages list
# Version 2.3: 18th March 2014
#     - Fixed macros
#     - Add LFD312
# Version 2.4: 20th March 2014
#     - Add support for running extra code as a hook
# Version 2.5: 25th March 2014
#     - Add openSuSE support, tested on 13.1
#     - Add CPUFLAGS support
#     - Add CONFIGS support
# Version 2.6: 28th March 2014
#     - Fix Red Hat distro detection
#===============================================================================
VERSION=2.6
#===============================================================================
#
# You can define requirements for a particular course by defining the following
# variables where LFXXXX is your course number:
#
#   DESCRIPTION[LFXXXX]         # Name of the course
#   ARCH[LFXXXX]=x86_64         # Required CPU arch (optional)
#   CPUFLAGS[LFXXXX]="vmx aes"  # Required CPU flags
#   CPUS[LFXXXX]=2              # How many CPUS/cores are required
#   PREFER_CPUS[LFXXXX]=4       # How many CPUS would be preferred
#   BOGOMIPS[LFXXXX]=4000       # How many cululative BogoMIPS you need
#   RAM[LFXXXX]=2               # How many GiB of RAM you need
#   DISK[LFXXXX]=30             # How many GiB of Disk you need free in $HOME
#   DISTRO_ARCH[LFXXXX]=x86_64  # Required Linux distro arch (optional)
#   INTERNET[LFXXXX]=y          # Is internet access required? (optional)
#   RUNCODE[LFXXXX]=lfXXX_code  # Run this bash function after install
#   DISTROS[LFXXXX]="Fedora-18+ CentOS-6+ Ubuntu-12.04+"
#                               # List of distros you can support.
#                               #   DistroName
#                               #   DistroName:arch
#                               #   DistroName-release
#                               #   DistroName-release+
#                               #   DistroName:arch-release
#                               #   DistroName:arch-release+
#
# Note: I know BogoMIPS aren't a great measure of CPU speed, but it's what we have
# easy access to.
#
# You can also specify required packages for your distro. All the appropriate
# package lists for the running machine will be checked. This allows you to
# keep package lists for particular distros, releases, arches and classes.
# For example:
#
#   PACKAGES[Ubuntu]="gcc less"
#   PACKAGES[Ubuntu_LFD320]="stress trace-cmd"
#   PACKAGES[Ubuntu-10.04]="git-core"
#   PACKAGES[Ubuntu-12.04]="git"
#   PACKAGES[Ubuntu-12.04_LFD411]="gparted u-boot-tools"
#   PACKAGES[RHEL]="gcc less"
#   PACKAGES[RHEL-6]="git"
#   PACKAGES[RHEL-6_LF320]="trace-cmd"
#
# Missing packages are listed so the user can install them manually, or you can
# rerun this script with --install to do it automatically.
#
# Support for all distros is not yet finished, but I've templated in code where
# possible. If you can add code to support a distro, please send a patch!
#
# If you want to see extra debug output, set DEBUG=1
#
#    DEBUG=1 ./ready-for.sh LFD411
#

#===============================================================================
usage() {
    echo "Usage: `basename $0` [options] <course>"
    echo "    --distro   List current Linux distro"
    echo "    --install  Install missing packages for the course"
    echo "    --list     List all supported courses"
    echo "    --version  List script version"
    echo
    echo "Example: `basename $0` --install LFD411"
    exit 0
}

#===============================================================================
debug() {
    [[ -z "$DEBUG" ]] || echo D: $* >&2
}

#===============================================================================
# Command option parsing
while [[ $# -gt 0 ]] ; do
    case "$1" in
        --debug) DEBUG=y ;;
        --distro) LIST_DISTRO=y; COURSE=None ;;
        -i|--install) INSTALL=y ;;
        --json) JSON=y; COURSE=None ;;
        -l|--list) LIST_COURSES=y; COURSE=None; break ;;
        -L|--list-requirements) LIST_REQS=y; [[ -n "$2" ]] || COURSE=None ;;
        --warn) OPT_WARN=y ;;
        --simulate-failure) SIMULATE_FAILURE=y ;;
        LFD*|LFS*) COURSE=${COURSE:-$1};;
        [0-9][0-9][0-9]) COURSE=LFD$1;;
        -v|--version) echo $VERSION; exit 0;;
        -h*|--help*|*) usage ;;
    esac
    shift
done
[[ -n "$COURSE" ]] || usage

#===============================================================================
# Make associative arrays
declare -A ARCH BOGOMIPS CONFIGS COURSE CPUFLAGS CPUS CLASS_ALIASES DEBREL \
    DESCRIPTION DISK DISTROS DISTS DISTRO_ARCH FALLBACK INTERNET PACKAGES \
    PREFER_CPUS RAM RUNCODE

#===============================================================================
#=== Start of Course Definitions ===============================================
#===============================================================================

#===============================================================================
# If we can't find settings/packages for a distro fallback to the next one
FALLBACK=(
    [CentOS]="RHEL"
    [Debian]="Ubuntu"
    [Debian-6]="Ubuntu-10.04"
    [Debian-7]="Ubuntu-12.04"
    [Fedora]="RHEL"
    [Kubuntu]="Ubuntu"
    [LinuxMint]="Ubuntu"
    [Mint]="Ubuntu"
    [RHEL]="CentOS"
    [SLES]="openSUSE"
    [SUSE]="openSUSE"
    [Suse]="openSUSE"
    [Ubuntu]="Debian"
    [XUbuntu]="Ubuntu"
    [openSUSE]="SLES"
)

#===============================================================================
# Distro release code names
DEBREL=(
    [hamm]=2
    [slink]=2.1
    [potato]=2.2
    [woody]=3
    [sarge]=3.1
    [etch]=4
    [lenny]=5
    [squeeze]=6
    [wheezy]=7
    [jessie]=8
    [sid]=999
    [unstable]=999
)

#===============================================================================
# Some classes have the same requirements as other classes
CLASS_ALIASES=([LFD404]=LFD405 [LFD410]=LFD411 [LFD414]=LFD415)
COURSE=${CLASS_ALIASES[${COURSE:=None}]:-$COURSE}

#===============================================================================
# Default Requirements for all courses
#===============================================================================
#ARCH=x86_64
#CPUFLAGS=
CPUS=1
PREFER_CPUS=2
BOGOMIPS=2000
RAM=1
DISK=5
#CONFIGS=
#DISTRO_ARCH=x86_64
#INTERNET=y
#DISTROS="Arch-2012+ CentOS-6+ Debian-6+ Fedora-18+ Gentoo-2+ LinuxMint-14+ openSUSE-12.2+ RHEL-6+ Ubuntu-12.04+"
DISTROS="CentOS-6+ Debian-6+ Fedora-18+ LinuxMint-14+ openSUSE-12.2+ RHEL-6+ Ubuntu-12.04+"
#===============================================================================
PACKAGES[Debian]="build-essential wget"
PACKAGES[Debian-6]=
PACKAGES[Debian-7]=
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu]=${PACKAGES[Debian]}
PACKAGES[Ubuntu-10.04]=
PACKAGES[Ubuntu-12.04]=
#-------------------------------------------------------------------------------
PACKAGES[openSUSE]="gcc wget"
PACKAGES[openSUSE-12.2]=
#-------------------------------------------------------------------------------
PACKAGES[RHEL]=${PACKAGES[openSUSE]}
PACKAGES[RHEL-6]=
#-------------------------------------------------------------------------------
# Add more default package lists here for other distros
#===============================================================================

#===============================================================================
# Common packages used in various courses
#===============================================================================
PACKAGES[@common]="autoconf automake bison curl flex gawk gdb gnupg gperf make
    mc patch screen sudo texinfo unzip vim zip"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu_@common]="g++-multilib libc6-dev texinfo tofrodos"
PACKAGES[Ubuntu-10.04_@common]="git-core"
PACKAGES[Ubuntu-12.04_@common]="git"
#-------------------------------------------------------------------------------
PACKAGES[Debian_@common]="${PACKAGES[Ubuntu_@common]}"
PACKAGES[Debian-6_@common]="${PACKAGES[Ubuntu-10.04_@common]}"
PACKAGES[Debian-7_@common]="${PACKAGES[Ubuntu-12.04_@common]}"
#-------------------------------------------------------------------------------
PACKAGES[openSUSE_@common]="dos2unix git glibc-devel texinfo"
#-------------------------------------------------------------------------------
PACKAGES[RHEL_@common]=${PACKAGES[openSUSE_@common]}

#===============================================================================
# Embedded packages
#===============================================================================
PACKAGES[@embedded]="minicom"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu-10.04_@embedded]="uboot-mkimage"
PACKAGES[Ubuntu-12.04_@embedded]="u-boot-tools"
#-------------------------------------------------------------------------------
PACKAGES[Debian-6_@embedded]="${PACKAGES[Ubuntu-10.04_@embedded]}"
PACKAGES[Debian-7_@embedded]="${PACKAGES[Ubuntu-12.04_@embedded]}"
#-------------------------------------------------------------------------------
PACKAGES[openSUSE_@embedded]="u-boot-tools"
#-------------------------------------------------------------------------------
PACKAGES[RHEL_@embedded]=${PACKAGES[openSUSE_@embedded]}

#===============================================================================
# Kernel related packages
#===============================================================================
PACKAGES[@kernel]="crash cscope gitk ketchup kexec-tools sparse stress
    sysfsutils sysstat"
#-------------------------------------------------------------------------------
PACKAGES[Debian_@kernel]="exuberant-ctags kdump-tools libfuse-dev libglade2-dev
    libgtk2.0-dev nfs-common symlinks"
PACKAGES[Ubuntu_@kernel]="${PACKAGES[Debian_@kernel]}
    libhugetlbfs-dev linux-crashdump"
PACKAGES[Ubuntu-10.04_@kernel]="lib32ncurses5-dev"
PACKAGES[Ubuntu-12.04_@kernel]="libncurses5-dev"
#-------------------------------------------------------------------------------
PACKAGES[Debian-6_@kernel]="${PACKAGES[Ubuntu-10.04_@kernel]}"
PACKAGES[Debian-7_@kernel]="${PACKAGES[Ubuntu-12.04_@kernel]}"
#-------------------------------------------------------------------------------
PACKAGES[RHEL_@kernel]="ctags fuse-devel gtk2-devel kdump libglade2-devel
    libhugetlbfs libhugetlbfs-libhugetlb-devel ncurses-devel nfs-client
    slang-devel"
PACKAGES[openSUSE_@kernel]="${PACKAGES[RHEL_@kernel]} yast2-kdump"

#===============================================================================
# Trace related packages
#===============================================================================
PACKAGES[@trace]="kernelshark iotop strace trace-cmd"

#===============================================================================
# Extra requirements for LFD312
#===============================================================================
DESCRIPTION[LFD312]="Developing Applications for Linux"
PACKAGES[LFD312]="valgrind kcachegrind"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu_LFD312]="electric-fence"
PACKAGES[Debian_LFD312]=${PACKAGES[Ubuntu_LFD312]}
PACKAGES[openSUSE_LFD312]="ElectricFence"
PACKAGES[RHEL_LFD312]=${PACKAGES[openSUSE_LFD312]}

#===============================================================================
# Extra requirements for LFD320
#===============================================================================
DESCRIPTION[LFD320]="Linux Kernel Internals and Debugging"
PACKAGES[LFD320]="@kernel @trace"

#===============================================================================
# Extra requirements for LFD331
#===============================================================================
DESCRIPTION[LFD331]="Developing Linux Device Drivers"
PACKAGES[LFD331]="@kernel @trace"

#===============================================================================
# Extra requirements for LFD405
#===============================================================================
DESCRIPTION[LFD405]="Building Embedded Linux with the Yocto Project"
CPUS[LFD405]=2
PREFER_CPUS[LFD405]=4
BOGOMIPS[LFD405]=4000
RAM[LFD405]=4
DISK[LFD405]=100
INTERNET[LFD405]=y
#-------------------------------------------------------------------------------
PACKAGES[LFD405]="@common @embedded diffstat chrpath xterm"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu_LFD405]="libsdl1.2-dev"
PACKAGES[Debian_LFD405]=${PACKAGES[Ubuntu_LFD405]}
PACKAGES[openSUSE_LFD405]="libSDL-devel"
PACKAGES[RHEL_LFD405]=${PACKAGES[openSUSE_LFD405]}

#===============================================================================
# Extra requirements for LFD411
#===============================================================================
DESCRIPTION[LFD411]="Embedded Linux Development"
CPUS[LFD411]=2
PREFER_CPUS[LFD411]=4
BOGOMIPS[LFD411]=3000
RAM[LFD411]=2
DISK[LFD411]=30
INTERNET[LFD411]=y
#-------------------------------------------------------------------------------
PACKAGES[LFD411]="@common @embedded @kernel @trace gparted"

#===============================================================================
# Extra requirements for LFD415
#===============================================================================
DESCRIPTION[LFD415]="Android Internals"
ARCH[LFD415]=x86_64
CPUS[LFD411]=2
PREFER_CPUS[LFD415]=4
BOGOMIPS[LFD415]=4000
RAM[LFD415]=4
DISK[LFD415]=80
DISTRO_ARCH[LFD415]=x86_64
INTERNET[LFD415]=y
DISTROS[LFD415]=Ubuntu:amd64-12.04+
RUNCODE[LFD415]=lfd415_extra
#===============================================================================
PACKAGES[LFD415]="@common @embedded vinagre"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu_LFD415]="libgl1-mesa-dev libxml2-utils mingw32
    python-markdown x11proto-core-dev xsltproc"
PACKAGES[Ubuntu-10.04_LFD415]="ant1.8 ia32-libs lib32readline5-dev lib32z-dev
    libx11-dev -zlib1g-dev"
PACKAGES[Debian-7_LFD415]="ant libreadline6 libsdl-image1.2 libx11-dev zlib1g-dev"
PACKAGES[Ubuntu-12.04_LFD415]="${PACKAGES[Debian-7_LFD415]} libreadline6-dev:i386
    libsdl-image1.2:i386 libx11-dev:i386 zlib1g-dev:i386"
#-------------------------------------------------------------------------------
PACKAGES[Debian_LFD415]="${PACKAGES[Ubuntu_LFD415]}"
PACKAGES[Debian-6_LFD415]="${PACKAGES[Ubuntu-10.04_LFD415]}"
#-------------------------------------------------------------------------------
PACKAGES[openSUSE_LFD415]="Mesa-devel libxml2-devel libxml2-tools
    python-Markdown xorg-x11-proto-devel ant glibc-32bit readline-devel"
#-------------------------------------------------------------------------------
PACKAGES[RHEL_LFD415]="${PACKAGES[openSUSE_LFD415]}"
#===============================================================================
lfd415_extra() {
    debug "Running ldf415_extra for $COURSE"
    GLSO=/usr/lib/i386-linux-gnu/libGL.so
    GLSOVER=/usr/lib/i386-linux-gnu/mesa/libGL.so.1
    if [[ -e $GLSOVER && ! -e $GLSO || -n "$SIMULATE_FAILURE" ]] ; then
        if [[ -z "$INSTALL" ]] ; then
            warn "Need to add missing symlink for libGL.so"
            highlight "You can do so by running '$0 --install $COURSE' or by:"
            dothis "  sudo ln -s $GLSOVER $GLSO"
        else
            warn "Adding missing symlink for libGL.so"
            sudo ln -s $GLSOVER $GLSO
        fi
    fi
}

#===============================================================================
# Extra requirements for LFD432
#===============================================================================
DESCRIPTION[LFD432]="Optimizing Device Drivers for Power Efficiency"
#-------------------------------------------------------------------------------
PACKAGES[LFD432]="@kernel @trace gkrellm xsensors"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu_LFD432]="lm-sensors"
PACKAGES[Debian_LFD432]=${PACKAGES[Ubuntu_LFD432]}
PACKAGES[openSUSE_LFD432]="sensors"
PACKAGES[RHEL_LFD432]=${PACKAGES[openSUSE_LFD432]}

#===============================================================================
# Extra requirements for LFS541
#===============================================================================
DESCRIPTION[LFS426]="Linux Performance Tuning"
ARCH[LFS426]=x86_64
CPUS[LFS426]=2
PREFER_CPUS[LFS426]=4
BOGOMIPS[LFS426]=20000
RAM[LFS426]=2
DISK[LFS426]=20
DISTRO_ARCH[LFS426]=x86_64
INTERNET[LFS426]=y
#-------------------------------------------------------------------------------
PACKAGES[LFS426]="@common @trace binutils bonnie++ collectl libtool m4
    memtest86+ sysstat"
PACKAGES[Debian_LFS426]="apache2-utils atop dstat g++ gfortran gnuplot htop
    iftop libaio-dev liblas-dev libncurses5-dev linux-tools ltrace nagios3
    zlib1g-dev"
PACKAGES[Ubuntu_LFS426]=${PACKAGES[Debian_LFS426]}
PACKAGES[openSUSE_LFS426]="blas-devel bonnie gcc-c++ gcc-fortran libaio-devel
    nagios ncurses-devel termcap zlib-devel zlib-devel-static"
PACKAGES[RHEL_LFS426]=${PACKAGES[openSUSE_LFS426]}

#===============================================================================
# Extra requirements for LFS541
#===============================================================================
DESCRIPTION[LFS541]="Introduction to Linux KVM Virtualization"
CPUS[LFS541]=2
BOGOMIPS[LFS541]=10000
RAM[LFS541]=2
CPUFLAGS[LFS541]=vmx
CONFIGS[LFS541]="HAVE_KVM KSM"
#-------------------------------------------------------------------------------
PACKAGES[LFS541]="@common qemu-kvm virt-manager automake"
#-------------------------------------------------------------------------------
PACKAGES[Ubuntu_LFS541]="libvirt-bin g++"
PACKAGES[Debian_LFS541]=${PACKAGES[Ubuntu_LFD432]}
PACKAGES[RHEL_LFS541]="libvirt gcc-c++ kernel-devel"
PACKAGES[openSUSE_LFS541]="${PACKAGES[RHEL_LFD432]} libvirt-client libvirt-daemon"

#===============================================================================
#=== End of Course Definitions =================================================
#===============================================================================

#===============================================================================
# List available courses
if [[ -n "$LIST_COURSES" ]] ; then
    echo "Available courses:"
    for D in ${!DESCRIPTION[*]}; do
        echo "  $D - ${DESCRIPTION[$D]}"
    done | sort
    exit 0
fi

#===============================================================================
RED="\e[0;31m"
GREEN="\e[0;32m"
YELLOW="\e[0;33m"
BLUE="\e[0;34m"
BACK="\e[0m"

pass() {
    echo -e "${GREEN}PASS${BACK}: $*"
}

warn() {
    echo -e "${YELLOW}WARN${BACK}: $*"
}

optwarn() {
    [[ -z "$OPT_WARN" ]] || warn $*
}

fail() {
    echo -e "${RED}FAIL${BACK}: $*"
}

highlight() {
    echo -e "${YELLOW}$*${BACK}"
}

dothis() {
    echo -e "${BLUE}$*${BACK}"
}

#===============================================================================
version_greater_equal () {
    local IFS=.
    local VER1=($1) VER2=($2)
    local i LEN=$( (( ${#VER1[*]} > ${#VER2[*]} )) && echo ${#VER1[*]} || echo ${#VER2[*]})
    for ((i=0; i<$LEN; i++)) ; do
        (( 10#${VER1[i]:-0} > 10#${VER2[i]:-0} )) && return 0
        (( 10#${VER1[i]:-0} < 10#${VER2[i]:-0} )) && return 1
    done
    return 0
}

#===============================================================================
distrib_list() {
    local RETURN=$1
    local L=$(for D in ${!PACKAGES[*]}; do echo $D; done | sed -e 's/_.*$//' | grep -- - | sort -u)
    eval $RETURN="'$L'"
}

#===============================================================================
# Chceck package list for obvious problems
package_list_check() {
    for PKG in ${!PACKAGES[@]}; do
        case "$PKG" in
            @*|*_@*) >/dev/null;;
            *@*) fail "'$PKG' is likely invalid. I think you meant '${PKG/@/_@}'";;
            *) >/dev/null;;
        esac
    done
}

#===============================================================================
package_list_expand() {
    debug package_list_expand "$*"
    local DID=$1; shift
    local DREL=$1; shift
    local PKG
    for PKG in $* ; do
        if [[ $PKG == @* ]] ; then
            package_list_expand $DID $DREL ${PACKAGES[$PKG]} \
                ${PACKAGES[${DID}_$PKG]} ${PACKAGES[$DID-${DREL}_$PKG]}
        else
            echo $PKG
        fi
    done
}

#===============================================================================
# Build package list
# TODO: Needs to be tested more with other distros
package_list() {
    local DISTID=$1
    local DISTREL=$2
    local CLASS=$3
    local RETURN=$4
    AVAIL_INDEXES=$(for D in ${!PACKAGES[*]}; do echo $D; done | grep $DISTID | sort -ru)
    debug "Available package indexes for $DISTID: $AVAIL_INDEXES"
    AVAIL_RELS=$(for R in $AVAIL_INDEXES; do R=${R#*-}; echo ${R%_*}; done | grep -v ^$DISTID | sort -ru)
    debug "Available distro releases for $DISTID: $AVAIL_RELS"
    local DVAR=1
    for R in $AVAIL_RELS ; do
        if version_greater_equal $R $DISTREL ; then
            DVAR=$R
            break
        fi
    done
    debug "We're going to use $DISTID-$DVAR"
    local PKGS
    for VAR in $CLASS $DISTID ${DISTID}_$CLASS $DISTID-$DVAR $DISTID-${DVAR}_$CLASS ; do
        PKGS+=" ${PACKAGES[$VAR]}"
    done
    debug "Initial package list for $DISTID-${DVAR}_$CLASS: $PKGS"
    PKGS=$(package_list_expand $DISTID $DVAR $PKGS | sort)
    debug "Final package list for $DISTID-${DVAR}_$CLASS: $PKGS"
    eval $RETURN="'$PKGS'"
}

#===============================================================================
# List information
list_entry() {
    local NAME=$1; shift
    [[ -z "$*" ]] || echo "    $NAME: $*"
}
list_array() {
    local NAME=$1; shift
    local WS=$1; shift
    local LIST=$*
    [[ -z "$LIST" ]] || echo "    $WS$NAME: $LIST"
}
list_sort() {
    sed 's/ /\n/g' <<< $* | sort
}

if [[ -n "$LIST_REQS" ]] ; then
    echo 'Courses:'
    [[ $COURSE = "None" ]] && COURSES=$(list_sort ${!DESCRIPTION[*]}) || COURSES=$COURSE
    for D in $COURSES; do
        echo "  $D:"
        list_entry DESCRIPTION ${DESCRIPTION[$D]}
        list_entry ARCH ${ARCH[$D]:-$ARCH}
        list_entry CPUFLAGS ${CPUFLAGS[$D]:-$CPUFLAGS}
        list_entry CPUS ${CPUS[$D]:-$CPUS}
        list_entry PREFER_CPUS ${PREFER_CPUS[$D]:-$PREFER_CPUS}
        list_entry BOGOMIPS ${BOGOMIPS[$D]:-$BOGOMIPS}
        list_entry RAM ${RAM[$D]:-$RAM}
        list_entry DISK ${DISK[$D]:-$DISK}
        list_entry CONFIGS ${CONFIGS[$D]:-$CONFIGS}
        list_entry DISTRO_ARCH ${DISTRO_ARCH[$D]:-$DISTRO_ARCH}
        list_entry INTERNET ${INTERNET[$D]:-$INTERNET}
        list_array DISTROS "" ${DISTROS[$D]:-$DISTROS}
        distrib_list DISTS
	debug "DISTS=$DISTS"
        if [[ -n "$DISTS" ]] ; then
            echo '    PACKAGES:'
            for DIST in $DISTS; do
                package_list ${DIST/-/ } $D P
                list_array $DIST "  " $P
            done
        fi
    done
    exit 0
fi

#===============================================================================
# JSON support
json_entry() {
    local NAME=$1; shift
    [[ -z "$*" ]] || echo "      \"$NAME\": \"$*\","
}
json_array() {
    local NAME=$1; shift
    local WS=$1; shift
    local LIST=$*
    [[ -z "$LIST" ]] || echo "      $WS\"$NAME\": [\"${LIST// /\", \"}\"],"
}

if [[ -n "$JSON" ]] ; then
    distrib_list DISTS
    ( cat <<END
{
  "whatisthis": "Description of Linux Foundation course requirements",
  "form": {
    "description": "Course description",
    "cpus": "Number of CPUs",
    "prefer_cpus": "Preferred number of CPUs",
    "bogomips": "CPU performance",
    "ram": "Amount of RAM",
    "disk": "Amount of free Disk space in \$HOME",
    "arch": "CPU architecture",
    "configs": "Kernel configuration options",
    "distro_arch": "Distro architecture",
    "internet": "Internet availability",
    "distros": "Distro",
    "packages": "Package list"
  },
  "distros": [
$(for DIST in $DISTS ; do echo '    "'$DIST'+",'; done | sort)
  ],
  "distro_fallback": {
$(for FB in ${!FALLBACK[*]} ; do echo '    "'$FB'": "'${FALLBACK[$FB]}'",'; done | sort)
  },
  "courses": {
END
    for D in $(list_sort ${!DESCRIPTION[*]}); do
        echo "    \"$D\": {"
        json_entry description ${DESCRIPTION[$D]}
        json_entry arch ${ARCH[$D]:-$ARCH}
        json_entry cpuflags ${CPUFLAGS[$D]:-$CPUFLAGS}
        json_entry cpus ${CPUS[$D]:-$CPUS}
        json_entry prefer_cpus ${PREFER_CPUS[$D]:-$PREFER_CPUS}
        json_entry bogomips ${BOGOMIPS[$D]:-$BOGOMIPS}
        json_entry ram ${RAM[$D]:-$RAM}
        json_entry disk ${DISK[$D]:-$DISK}
        json_entry configs ${CONFIGS[$D]:-$CONFIGS}
        json_entry distro_arch ${DISTRO_ARCH[$D]:-$DISTRO_ARCH}
        json_entry internet ${INTERNET[$D]:-$INTERNET}
        json_array distros "" ${DISTROS[$D]:-$DISTROS}
        if [[ -n "$DISTS" ]] ; then
            echo '      "packages": {'
            for DIST in $DISTS; do
                package_list ${DIST/-/ } $D P
                json_array $DIST "  " $P
            done
            echo '      },'
        fi
        echo '    },'
    done
    echo '  }'
    echo '}') | perl -e '$/=undef; $_=<>; s/,(\s+[}\]])/$1/gs; print;'
    exit 0
fi

#===============================================================================
ARCH=${ARCH[$COURSE]:-$ARCH}
CPUFLAGS=${CPUFLAGS[$COURSE]:-${CPUFLAGS}}
CPUS=${CPUS[$COURSE]:-$CPUS}
PREFER_CPUS=${PREFER_CPUS[$COURSE]:-${PREFER_CPUS:-$CPUS}}
RAM=${RAM[$COURSE]:-$RAM}
DISK=${DISK[$COURSE]:-$DISK}
CONFIGS=${CONFIGS[$COURSE]:-$CONFIGS}
INTERNET=${INTERNET[$COURSE]:-$INTERNET}
DISTROS=${DISTROS[$COURSE]:-$DISTROS}
DISTRO_ARCH=${DISTRO_ARCH[$COURSE]:-$DISTRO_ARCH}
#===============================================================================
debug COURSE=$COURSE
debug ARCH=$ARCH
debug CPUFLAGS=$CPUFLAGS
debug CPUS=$CPUS
debug PREFER_CPUS=$PREFER_CPUS
debug BOGOMIPS=$BOGOMIPS
debug RAM=$RAM
debug DISK=$DISK
debug CONFIGS=$CONFIGS
debug INTERNET=$INTERNET
debug DISTROS=$DISTROS
debug DISTRO_ARCH=$DISTRO_ARCH

#===============================================================================
# Determine Distro and release
if [[ -f /etc/lsb-release ]] ; then
    . /etc/lsb-release
fi
#-------------------------------------------------------------------------------
if [[ -z "$DISTRIB_ID" ]] ; then
    if [[ -f /usr/bin/lsb_release ]] ; then
        DISTRIB_ID=$(lsb_release -is)
        DISTRIB_RELEASE=$(lsb_release -rs)
        DISTRIB_CODENAME=$(lsb_release -cs)
        DISTRIB_DESCRIPTION=$(lsb_release -ds)
    fi
fi
#-------------------------------------------------------------------------------
if [[ -z "$DISTRIB_ID" ]] ; then
    if [[ -f /etc/debian_version ]] ; then
        DISTRIB_ID=Debian
        DISTRIB_CODENAME=$(sed 's|^.*/||' /etc/debian_version)
        DISTRIB_RELEASE=${DEBREL[$DISTRIB_CODENAME]:-$DISTRIB_CODENAME}
#-------------------------------------------------------------------------------
    elif [[ -f /etc/SuSE-release ]] ; then
        DISTRIB_ID=$(awk 'NR==1 {print $1}' /etc/SuSE-release)
        DISTRIB_RELEASE=$(awk '/^VERSION/ {print $3}' /etc/SuSE-release)
        DISTRIB_CODENAME=$(awk '/^CODENAME/ {print $3}' /etc/SuSE-release)
#-------------------------------------------------------------------------------
    elif [[ -f /etc/redhat-release ]] ; then
        if egrep -q "^Red Hat Enterprise Linux" /etc/redhat-release ; then
            DISTRIB_ID=RHEL
            DISTRIB_RELEASE=$(awk '{print $7}' /etc/redhat-release)
            DISTRIB_CODENAME=$(sed 's/^.*(//; s/)*$//;' /etc/redhat-release)
        elif egrep -q "^CentOS|Fedora" /etc/redhat-release ; then
            DISTRIB_ID=$(awk '{print $1}' /etc/redhat-release)
            DISTRIB_RELEASE=$(awk '{print $3}' /etc/redhat-release)
            DISTRIB_CODENAME=$(sed 's/^.*(//; s/)*$//;' /etc/redhat-release)
        else
            DISTRIB_ID=RHEL
        fi
#-------------------------------------------------------------------------------
    elif [[ -f /etc/os-release ]] ; then
        . /etc/os-release
        DISTRIB_ID=${ID^}
        if [[ -e /etc/arch-release ]] ; then
            # Arch Linux doesn't have a "release"...
            # So instead we'll look at the modification date of pacman
            DISTRIB_RELEASE=$(ls -l --time-style=+%Y.%m /bin/pacman | cut -d' ' -f6)
#-------------------------------------------------------------------------------
        elif [[ -e /etc/gentoo-release ]] ; then
            # Arch Linux doesn't have a "release"...
            # So instead we'll look at the Base System release
            DISTRIB_RELEASE=$(cut -d' ' -f5 /etc/gentoo-release)
        fi
        DISTRIB_DESCRIPTION=$PRETTY_NAME
    fi
fi
#-------------------------------------------------------------------------------
if [[ $DISTRIB_ID =~ 'Debian' ]] ; then
    DISTRIB_RELEASE=${DEBREL[$DISTRIB_RELEASE]:-$DISTRIB_RELEASE}
elif [[ $DISTRIB_ID =~ 'SUSE' ]] ; then
    DISTRIB_ID=$(echo $DISTRIB_DESCRIPTION | sed 's/"//g; s/ .*//')
elif [[ $DISTRIB_ID == RedHatEnterprise* ]] ; then
    DISTRIB_ID=RHEL
fi
#-------------------------------------------------------------------------------
DISTRIB_ID=${DISTRIB_ID:-Unknown}
DISTRIB_RELEASE=${DISTRIB_RELEASE:-0}
DISTRIB_CODENAME=${DISTRIB_CODENAME:-Unknown}
[[ -n "$DISTRIB_DESCRIPTION" ]] || DISTRIB_DESCRIPTION="$DISTRIB_ID $DISTRIB_RELEASE"

#===============================================================================
# Fallback to other distro if settings/packages aren't available for distrib id
fallback() {
    local RETURN=$1
    local DIST=$2
    if [[ -z "${PACKAGES[$DIST]}" ]] ; then
        eval $RETURN="'${FALLBACK[$DIST]}'"
    else
        eval $RETURN="'$DIST'"
    fi
}
fallback EFFECTIVE_DISTRIB_ID $DISTRIB_ID
#debug "0: EFFECTIVE_DISTRIB_ID=$EFFECTIVE_DISTRIB_ID"
fallback EFFECTIVE_DISTRIB_ID $EFFECTIVE_DISTRIB_ID
#debug "1: EFFECTIVE_DISTRIB_ID=$EFFECTIVE_DISTRIB_ID"
if [[ $EFFECTIVE_DISTRIB_ID =~ '-' ]] ; then
    read EFFECTIVE_DISTRIB_ID EFFECTIVE_DISTRIB_RELEASE <<<${EFFECTIVE_DISTRIB_ID/-/ }
else
    EFFECTIVE_DISTRIB_RELEASE=1
fi
	
#===============================================================================
# Determine Distro arch
if [[ -e /usr/bin/dpkg && $DISTRIB_ID =~ Debian|Kubuntu|LinuxMint|Mint|Ubuntu|Xubuntu ]] ; then
    DARCH=$(dpkg --print-architecture)
elif [[ -e /bin/rpm || -e /usr/bin/rpm ]] && [[ $DISTRIB_ID =~ CentOS|Fedora|RHEL|openSUSE|Suse|SUSE ]] ; then
    DARCH=$(rpm --eval %_arch)
elif [[ -e /usr/bin/file ]] ; then
    DARCH=$(/usr/bin/file /usr/bin/file | cut -d, -f2)
    DARCH=${DARCH## }
    DARCH=${DARCH/-64/_64}
else
    DARCH=Unknown
fi
# Because Debian and derivatives use amd64 instead of x86_64...
if [[ "$DARCH" == "amd64" ]] ; then
    DISTRIB_ARCH=x86_64
else
    DISTRIB_ARCH=$DARCH
fi

#===============================================================================
debug DISTRIB_ID=$DISTRIB_ID
debug DISTRIB_RELEASE=$DISTRIB_RELEASE
debug DISTRIB_CODENAME=$DISTRIB_CODENAME
debug DISTRIB_DESCRIPTION=$DISTRIB_DESCRIPTION
debug EFFECTIVE_DISTRIB_ID=$EFFECTIVE_DISTRIB_ID
debug EFFECTIVE_DISTRIB_RELEASE=$EFFECTIVE_DISTRIB_RELEASE
debug DISTRIB_CODENAME=$DISTRIB_CODENAME
debug DISTRIB_ARCH=$DISTRIB_ARCH
debug DARCH=$DARCH
#===============================================================================
if [[ -n "$LIST_DISTRO" ]] ; then
    echo "Linux Distro: $DISTRIB_ID:$DISTRIB_ARCH-$DISTRIB_RELEASE ($DISTRIB_CODENAME)"
    exit 0
fi

#===============================================================================
# Build package list
package_list_check
package_list $EFFECTIVE_DISTRIB_ID $EFFECTIVE_DISTRIB_RELEASE $COURSE PACKAGES

#===============================================================================
echo "Checking that this computer is suitable for building $COURSE: ${DESCRIPTION[$COURSE]}"
[[ -n ${DESCRIPTION[$COURSE]} ]] || warn "Invalid course \"$COURSE\", checking defaults requirements instead"

#===============================================================================
# Right cpu architecture?
if [[ -n "$ARCH" ]] ; then
    CPU_ARCH=$(uname -m)
    if [[ -n "$ARCH" && $CPU_ARCH != "$ARCH" || -n "$SIMULATE_FAILURE" ]] ; then
        fail "CPU architecture is $CPU_ARCH (Must be $ARCH)"
        FAILED=y
    else
        pass "CPU architecture is $CPU_ARCH"
    fi
fi

#===============================================================================
# Right cpu flags?
if [[ -n "$CPUFLAGS" ]] ; then
    for FLAG in $CPUFLAGS ; do
        grep -qc " $FLAG " /proc/cpuinfo || NOTFOUND+=" $FLAG"
    done
    if [[ -n "$NOTFOUND" ]] ; then
        fail "CPU doesn't have the following capabilities:$NOTFOUND"
        FAILED=y
    else
        pass "CPU has all needed capabilities: $CPUFLAGS"
    fi
fi

#===============================================================================
# Enough CPUS?
NUM_CPU=$(lscpu | awk '/^CPU\(s\):/ {print $2}')
if [[ $NUM_CPU -lt $CPUS || -n "$SIMULATE_FAILURE" ]] ; then
    fail "Single core CPU: not powerful enough (require at least $CPUS, though $PREFER_CPUS is preferred)"
    FAILED=y
    NOTENOUGH=y
elif [[ $NUM_CPU -lt $PREFER_CPUS ]] ; then
    pass "$NUM_CPU core CPU (good enough but $PREFER_CPUS is preferred)"
else
    pass "$NUM_CPU core CPU"
fi

#===============================================================================
# Enough BogoMIPS?
if [[ -n "$BOGOMIPS" ]] ; then
    MIPS=$(cat /proc/cpuinfo | awk '/^bogomips/ {mips+=$3} END {print int(mips + 0.5)}')
    if [[ $MIPS -lt $BOGOMIPS || -n "$SIMULATE_FAILURE" ]] ; then
        fail "Your CPU isn't powerful enough (must be at least $BOGOMIPS BogoMIPS cumulatively)"
        FAILED=y
    else
        if [[ -n "$NOTENOUGH" ]] ; then
            warn "Despite not having enough CPUs, you may still have enough speed (currently at $MIPS BogoMIPS)"
        else
            pass "Your CPU appears powerful enough (currently at $MIPS BogoMIPS cumulatively)"
        fi
    fi
fi

#===============================================================================
# Enough RAM?
RAM_GBYTES=$(awk '/^MemTotal/ {print int($2/1024/1024+0.7)}' /proc/meminfo)
if [[ $RAM_GBYTES -lt $RAM || -n "$SIMULATE_FAILURE" ]] ; then
    fail "Only $RAM_GBYTES GiB RAM (require at least $RAM GiB)"
    FAILED=y
else
    pass "$RAM_GBYTES GiB RAM"
fi

#===============================================================================
# Enough free disk space in $BUILDHOME? (defaults to $HOME)
DISK_GBYTES=$(df ${BUILDHOME:-$HOME} | awk '{if (NR == 2) print int(($4+524288)/1048576)}')
if [[ ${DISK_GBYTES:=1} -le $DISK || -n "$SIMULATE_FAILURE" ]] ; then
    fail "only $DISK_GBYTES GiB free in ${BUILDHOME:-$HOME} (need at least $DISK GiB) Set BUILDHOME=/path/to/disk to override \$HOME"
    FAILED=y
else
    pass "$DISK_GBYTES GiB free disk space in $HOME"
fi

#===============================================================================
# Right Linux distribution architecture?
if [[ -n "$DISTRO_ARCH" ]] ; then
    if [[ "$DISTRIB_ARCH" != "$DISTRO_ARCH" || -n "$SIMULATE_FAILURE" ]] ; then
        fail "The distribution architecture must be $DISTRO_ARCH"
        FAILED=y
    else
        pass "Linux distribution architecture is $DISTRO_ARCH"
    fi
fi

#===============================================================================
# Right Linux distribution?
# TODO: Needs to be tested more with other distros
if [[ -z "$DISTROS" ]] ; then
    warn "No required Linux distribution (Currently running $DISTRIB_DESCRIPTION)"
else
    for DISTRO in $DISTROS ; do
        [[ $DISTRO = *+ ]] && G=y && DISTRO=${DISTRO%\+} || unset G
        [[ $DISTRO = *-* ]] && R=${DISTRO#*-} && DISTRO=${DISTRO%-*} || R='*'
        [[ $DISTRO = *:* ]] && A=${DISTRO#*:} && DISTRO=${DISTRO%:*} || A='*'
        debug "Are we running DISTRO=$DISTRO ARCH=$A REL=$R or-newer=$G"
        [[ -n "$G" && "$R" != "*" ]] && version_greater_equal $DISTRIB_RELEASE $R && R='*'
        if [[ $DISTRIB_ID = $DISTRO && $DARCH = $A && $DISTRIB_RELEASE = $R ]] ; then
            FOUND=1
            break
        fi
    done
    if [[ -n "$FOUND" && -z "$SIMULATE_FAILURE" ]] ; then
        pass "Linux distribution is $DISTRIB_ID:$DARCH-$DISTRIB_RELEASE ${DISTRIB_CODENAME:+($DISTRIB_CODENAME)}"
    else
        fail "The distribution must be: $DISTROS"
        FAILED=y
    fi
fi

#===============================================================================
# Is the kernel configured properly?
if [[ -n "$CONFIGS" ]] ; then
    for CONFIG in $CONFIGS ; do
        grep -qc CONFIG_$CONFIG /boot/config-$(uname -r) || MISSINGCONFIG+=" $CONFIG"
    done
    if [[ -z "$MISSINGCONFIG" ]] ; then
        pass "The Current kernel is properly configured ($CONFIGS)"
    else
        fail "Current kernel is missing these options:$MISSINGCONFIG"
        FAILED=y
    fi
fi

#===============================================================================
# Is there Internet?
if [[ $INTERNET =~ [yY].* ]] ; then
    PING_HOST=${PING_HOST:-8.8.8.8}
    if ping -q -c 1 $PING_HOST >/dev/null 2>&1 && [[ -z "$SIMULATE_FAILURE" ]] ; then
        pass "Internet is available"
    else
        fail "Internet doesn't appear to be available (but then again, all I did was try to ping $PING_HOST)"
        FAILED=y
    fi
else
    optwarn "Not requiring Internet availability"
fi

#===============================================================================
# Run extra code based on distro, release, and course
run_extra_code() {
    for KEY in $COURSE \
            $EFFECTIVE_DISTRIB_ID \
            ${EFFECTIVE_DISTRIB_ID}_$COURSE \
            $EFFECTIVE_DISTRIB_ID-$EFFECTIVE_DISTRIB_RELEASE \
            $EFFECTIVE_DISTRIB_ID-${EFFECTIVE_DISTRIB_RELEASE}_$COURSE ; do
        if [[ -n "${RUNCODE[$KEY]}" ]] ; then 
            debug "run exra setup code for $KEY -> eval ${RUNCODE[$KEY]}"
            eval "${RUNCODE[$KEY]}"
	fi
    done
}

#===============================================================================
debinstall() {
    local PACKAGES=$*
    [[ -z "$PACKAGES" ]] && return
    debug PACKAGES=$PACKAGES

    APTGET="apt-get --no-install-recommends"
    ERRPKG=$($APTGET --dry-run install $PACKAGES 2>&1 | awk '/^E: / {print $6}')
    if [[ -n "$ERRPKG" ]] ; then
        fail "Can't find package(s) in index: $ERRPKG"
        echo "Looks like you need to run 'sudo apt-get update' and try this again"
        exit
    fi
    NEWPKG=$($APTGET --dry-run install $PACKAGES | awk '/^Inst / {print $2}')
    [[ -n "$SIMULATE_FAILURE" ]] && NEWPKG=$PACKAGES
    debug NEWPKG=$NEWPKG
    if [[ -z "$NEWPKG" ]] ; then
        pass "All required packages are already installed"
    else
        warn "Need to install:" $NEWPKG
        if [[ -z "$INSTALL" ]] ; then
            highlight "You can do so by running '$0 --install $COURSE' or by:"
            dothis "  sudo $APTGET install" $NEWPKG
            MISSING_PACKAGES=y
        else
            sudo $APTGET install $NEWPKG
            FAILPKG=$(sudo $APTGET --dry-run install $PACKAGES | awk '/^Conf / {print $2}')
            if [[ -n "$FAILPKG" ]] ; then
                fail "Some packages didn't install: $FAILPKG"
                FAILED=y
            else
                pass "All required packages are now installed"
            fi
        fi
    fi
}

#===============================================================================
rpminstall() {
    local TOOL=$1; shift
    local PACKAGES=$*
    [[ -z "$PACKAGES" ]] && return

    NEWPKG=$(rpm -q $PACKAGES | awk '/is not installed$/ {print $2}')
    [[ -n "$SIMULATE_FAILURE" ]] && NEWPKG=$PACKAGES
    if [[ -z "$NEWPKG" ]] ; then
        pass "All required packages are already installed"
    else
        warn "Need to install:" $NEWPKG
        if [[ -z "$INSTALL" ]] ; then
            highlight "You can do so by running '$0 --install $COURSE' or by:"
            dothis "  sudo $TOOL install" $NEWPKG
            MISSING_PACKAGES=y
        else
            sudo $TOOL install $NEWPKG
            PAILPKG=$(rpm -q $PACKAGES | awk '/is not installed$/ {print $2}')
            if [[ -n "$FAILPKG" ]] ; then
                fail "Some packages didn't install: $FAILPKG"
                FAILED=y
            else
                pass "All required packages are now installed"
            fi
        fi
    fi
}

#===============================================================================
if [[ $DISTRIB_ID == "Debian" || $DISTRIB_ID == "Ubuntu" ]]  ; then
    debinstall $PACKAGES
    run_extra_code

#===============================================================================
elif [[ $DISTRIB_ID == "CentOS" || $DISTRIB_ID == "Fedora" || $DISTRIB_ID == "RHEL" ]]  ; then
    rpminstall yum $PACKAGES
    run_extra_code

#===============================================================================
elif [[ $DISTRIB_ID == "openSUSE" || $DISTRIB_ID == "Suse" ]]  ; then
    rpminstall zypper $PACKAGES
    run_extra_code

#===============================================================================
elif [[ $DISTRIB_ID == "Arch" ]]  ; then
# TODO: Add support for pacman here to provide similar functionality as apt-get code above
    warn "Currently there is no package support for Arch Linux"
    run_extra_code

#===============================================================================
elif [[ $DISTRIB_ID == "Gentoo" ]]  ; then
# TODO: Add support for emerge here to provide similar functionality as apt-get code above
    warn "Currently there is no package support for Gentoo"
    run_extra_code
fi

#===============================================================================
# Overall PASS/FAIL
echo
if [[ -n "$FAILED" ]] ; then
    fail "You likely won't be able to complete the course on this computer unless you can fix the above failures"
else
    [[ -n "$MISSING_PACKAGES" ]] && warn "You have more packages to install (listed above), but otherwise..."
    pass "You are ready for the course! W00t!"
fi

exit 0
